import * as React from "react";
import {Square} from "./square";

interface IBoardProps {
    squares: string[];
    wonSquares: number[];
    onClick: (i: number) => void;
}

export class Board extends React.Component<IBoardProps, {}> {

    renderSquare(i: number) {
        const { wonSquares } = this.props;
        const wonClass = wonSquares &&
                (wonSquares[0] === i || wonSquares[1] === i || wonSquares[2] === i)
                ? "won"
                : "";
        const { squares } = this.props;
        return <Square key={i} class={wonClass} value={squares[i]} onClick={() => this.props.onClick(i)} />;
    }

    render(): JSX.Element {
        const board = [];
        let keyIndex = 0;
        for (let row = 0; row < 3; row++) {
            const columns = [];
            for (let square = 0; square < 3; square++) {
                columns.push(this.renderSquare(keyIndex++));
            }
            board.push(<div key={row} className="board-row">{columns}</div>);
        }
        return (
            <div>{board}</div>
        );
    }
}
