
import * as React from "react";

interface ISquareProps {
    value: string;
    onClick: () => void;
    class: string;
}

export const Square = (props: ISquareProps) => {
    return (
        <button className={"square" + " " + props.class} onClick={props.onClick}>
            {props.value}
        </button>
    );
};
