import * as React from "react";
import { Board } from "./board";
import "./index.css";

interface ISquares {
    squares: string[];
    lastLocation: number;
}

interface IGameState {
    history: ISquares[];
    xIsNext: boolean;
    stepNumber: number;
    sorted: boolean;
}

interface IWinningResult {
    winner?: string;
    winningPattern?: number[];
}

export class Game extends React.Component<{}, IGameState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
                lastLocation: null
            }],
            xIsNext: true,
            stepNumber: 0,
            sorted: false
        };
    }

    handleClick = (i: number) => {
        const { xIsNext, stepNumber, history } = this.state;
        const newHistory = history.slice(0, stepNumber + 1);
        const squares = newHistory[newHistory.length - 1].squares.slice();

        if (this.calculateWinner().winner || squares[i]) {
            return;
        }

        squares[i] = xIsNext ? "X" : "O";
        this.setState({
            history: newHistory.concat({ squares, lastLocation: i }),
            stepNumber: newHistory.length,
            xIsNext: !xIsNext,
        });
    }

    calculateWinner = () => {
        const { history } = this.state;
        const { squares } = history[history.length - 1];
        const result: IWinningResult = {};
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        lines.forEach((line) => {
            const [a, b, c] = line;
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                result.winner = squares[a];
                result.winningPattern = line;
            }
        });
        return result;
    }

    reset = () => {
        this.setState({
            history: [{
                squares: Array(9).fill(null),
                lastLocation: null
            }],
            xIsNext: true,
            stepNumber: 0,
        });
    }

    jumpTo = (step: number) => {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        });
    }

    render(): JSX.Element {
        const { history, xIsNext, stepNumber, sorted } = this.state;
        const { squares } = history[stepNumber];
        const moves = history.map((step, move) => {
            const { lastLocation } = step;
            const col = lastLocation % 3;
            const row = (lastLocation - col) / 3;
            const desc = move ?
                `Go to move #${move} col:${col + 1} row: ${row + 1}` :
                `Go to game start`;
            return (
                <li key={move}>
                    <button className={stepNumber === move ? "active" : ""} onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });
        const result = this.calculateWinner();
        let status;
        if (result.winner) {
            status = `Winner: ${result.winner}`;
        } else {
            status = stepNumber === 9 ? "Draw!" : `Next Player: ${xIsNext ? "X" : "O"}`;
        }
        return (
            <div className="game">
                <div className="game-board">
                    <Board squares={squares} wonSquares={result.winner ? result.winningPattern : null} onClick={this.handleClick} />
                </div>
                <div className="game-info">
                    <div><button className={"resetButton"} onClick={this.reset}>Reset</button></div>
                    <div className="statusLabel">{status}</div>
                    <div><button className={"sortButton"} onClick={() => this.setState({ sorted: !sorted })}>Sort</button></div>
                    <ol>{sorted ? moves.reverse() : moves}</ol>
                </div>
            </div>
        );
    }
}
